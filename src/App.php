<?php

namespace Davek1312\App;

/**
 * Pulls together the components
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class App {

    /**
     * @var array
     */
    private $appConfig;
    /**
     * @var array
     */
    private $config;
    /**
     * @var array
     */
    private $migrations;
    /**
     * @var array
     */
    private $commands;
    /**
     * @var array
     */
    private $lang;
    /**
     * @var array
     */
    private $validationRules;
    /**
     * @var array
     */
    private $implicitValidationRules;
    /**
     * @var array
     */
    private $validationReplacers;
    /**
     * @var array
     */
    private $routes;

    /**
     * Returns all the registries components
     *
     * @param string $componentAccessor
     *
     * @return array
     */
    private function getComponents($componentAccessor) {
        $components = [];
        foreach($this->getRegistry() as $registryClass) {
            $registry = new $registryClass();
            $components = array_merge($components, $registry->{$componentAccessor}());
        }
        return $components;
    }

    /**
     * Returns the registry array located in the project's root folder
     *
     * @return string
     */
    public function getAppConfig() {
        if(!$this->appConfig) {
            $this->appConfig = include __DIR__.'/../../../../davek1312/app.php';
        }
        return $this->appConfig;
    }

    /**
     * @param array $appConfig
     */
    public function setAppConfig($appConfig) {
        $this->appConfig = $appConfig;
    }

    /**
     * @return array
     */
    public function getRegistry() {
        return $this->getFromAppConfig('registry');
    }

    /**
     * @return array
     */
    public function getRootPackageDir() {
        return $this->getFromAppConfig('root_package_dir');
    }

    /**
     * Retrieve a value form the app array
     *
     * @param string $key
     *
     * @return mixed
     *
     */
    private function getFromAppConfig($key) {
        $appConfig = $this->getAppConfig();
        return isset($appConfig[$key]) ? $appConfig[$key] : null;
    }

    /**
     * @return array
     */
    public function getConfig() {
        if(!$this->config) {
            $this->config = $this->getComponents('getConfig');
        }
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config) {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getMigrations() {
        if(!$this->migrations) {
            $this->migrations = $this->getComponents('getMigrations');
        }
        return $this->migrations;
    }

    /**
     * @param array $migrations
     */
    public function setMigrations($migrations) {
        $this->migrations = $migrations;
    }

    /**
     * @return array
     */
    public function getCommands() {
        if(!$this->commands) {
            $this->commands = $this->getComponents('getCommands');
        }
        return $this->commands;
    }

    /**
     * @param array $commands
     */
    public function setCommands($commands) {
        $this->commands = $commands;
    }

    /**
     * @return array
     */
    public function getLang() {
        if(!$this->lang) {
            $this->lang = $this->getComponents('getLang');
        }
        return $this->lang;
    }

    /**
     * @param array $lang
     */
    public function setLang($lang) {
        $this->lang = $lang;
    }

    /**
     * @return array
     */
    public function getValidationRules() {
        if(!$this->validationRules) {
            $this->validationRules = $this->getComponents('getValidationRules');
        }
        return $this->validationRules;
    }

    /**
     * @param array $validationRules
     */
    public function setValidationRules($validationRules) {
        $this->validationRules = $validationRules;
    }

    /**
     * @return array
     */
    public function getImplicitValidationRules() {
        if(!$this->implicitValidationRules) {
            $this->implicitValidationRules = $this->getComponents('getImplicitValidationRules');
        }
        return $this->implicitValidationRules;
    }

    /**
     * @param array $implicitValidationRules
     */
    public function setImplicitValidationRules($implicitValidationRules) {
        $this->implicitValidationRules = $implicitValidationRules;
    }

    /**
     * @return array
     */
    public function getValidationReplacers() {
        if(!$this->validationReplacers) {
            $this->validationReplacers = $this->getComponents('getValidationReplacers');
        }
        return $this->validationReplacers;
    }

    /**
     * @param array $validationReplacers
     */
    public function setValidationReplacers($validationReplacers) {
        $this->validationReplacers = $validationReplacers;
    }

    /**
     * @return array
     */
    public function getRoutes() {
        if(!$this->routes) {
            $this->routes = $this->getComponents('getRoutes');
        }
        return $this->routes;
    }

    /**
     * @param array $routes
     */
    public function setRoutes($routes) {
        $this->routes = $routes;
    }
}