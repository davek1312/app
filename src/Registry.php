<?php

namespace Davek1312\App;

/**
 * Registers davek1312 components
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
abstract class Registry {

    /**
     * @var array
     */
    private $config = [];
    /**
     * @var array
     */
    private $migrations = [];
    /**
     * @var array
     */
    private $commands = [];
    /**
     * @var array
     */
    private $lang = [];
    /**
     * @var array
     */
    private $validationRules = [];
    /**
     * @var array
     */
    private $implicitValidationRules = [];
    /**
     * @var array
     */
    private $validationReplacers = [];
    /**
     * @var array
     */
    private $routes = [];

    /**
     * Registry constructor.
     */
    public function __construct() {
        $this->register();
    }

    /**
     * Registers configuration files
     *
     * @param string|array $value
     *
     * @return void
     */
    protected function registerConfig($value) {
        $this->registerComponent('config', $value);
    }

    /**
     * Registers migration files
     *
     * @param string|array $value
     *
     * @return void
     */
    protected function registerMigrations($value) {
        $this->registerComponent('migrations', $value);
    }

    /**
     * Registers command files
     *
     * @param string|array $value
     *
     * @return void
     */
    protected function registerCommands($value) {
        $this->registerComponent('commands', $value);
    }

    /**
     * Registers package language file
     *
     * @param string $namespace
     * @param string $pathToLang
     *
     * @return void
     */
    protected function registerLang($namespace, $pathToLang) {
        $this->registerComponent('lang', [
            $namespace => $pathToLang,
        ]);
    }

    /**
     * Registers package validation rules
     *
     * @param string $name
     * @param \Closure|string $rule
     *
     * @return void
     */
    protected function registerValidationRules($name, $rule) {
        $this->registerComponent('validationRules', [
            $name => $rule,
        ]);
    }

    /**
     * Registers implicit package validation rules
     *
     * @param string $name
     * @param \Closure|string $rule
     *
     * @return void
     */
    protected function registerImplicitValidationRules($name, $rule) {
        $this->registerComponent('implicitValidationRules', [
            $name => $rule,
        ]);
    }

    /**
     * Registers package validation replaces
     *
     * @param string $name
     * @param \Closure|string $replacer
     *
     * @return void
     */
    protected function registerValidationReplacers($name, $replacer) {
        $this->registerComponent('validationReplacers', [
            $name => $replacer,
        ]);
    }

    /**
     * Registers route paths
     *
     * @param string|array $routePath
     *
     * @return void
     */
    protected function registerRoutes($routePath) {
        $this->registerComponent('routes', $routePath);
    }

    /**
     * Registers a component
     *
     * @param string $component
     * @param string|array $value
     *
     * @return void
     */
    private function registerComponent($component, $value) {
        $value = is_array($value) ? $value : [$value];
        $this->{$component} = array_merge($this->{$component}, $value);
    }

    /**
     * @return array
     */
    public function getConfig() {
        return $this->config;
    }

    /**
     * @param array $config
     */
    public function setConfig($config) {
        $this->config = $config;
    }

    /**
     * @return array
     */
    public function getMigrations() {
        return $this->migrations;
    }

    /**
     * @param array $migrations
     */
    public function setMigrations($migrations) {
        $this->migrations = $migrations;
    }

    /**
     * @return array
     */
    public function getCommands() {
        return $this->commands;
    }

    /**
     * @param array $commands
     */
    public function setCommands($commands) {
        $this->commands = $commands;
    }

    /**
     * @return array
     */
    public function getLang() {
        return $this->lang;
    }

    /**
     * @param array $lang
     */
    public function setLang($lang) {
        $this->lang = $lang;
    }

    /**
     * @return array
     */
    public function getValidationRules() {
        return $this->validationRules;
    }

    /**
     * @param array $validationRules
     */
    public function setValidationRules($validationRules) {
        $this->validationRules = $validationRules;
    }

    /**
     * @return array
     */
    public function getImplicitValidationRules() {
        return $this->implicitValidationRules;
    }

    /**
     * @param array $implicitValidationRules
     */
    public function setImplicitValidationRules($implicitValidationRules) {
        $this->implicitValidationRules = $implicitValidationRules;
    }

    /**
     * @return array
     */
    public function getValidationReplacers() {
        return $this->validationReplacers;
    }

    /**
     * @param array $validationReplacers
     */
    public function setValidationReplacers($validationReplacers) {
        $this->validationReplacers = $validationReplacers;
    }

    /**
     * @return array
     */
    public function getRoutes() {
        return $this->routes;
    }

    /**
     * @param array $routes
     */
    public function setRoutes($routes) {
        $this->routes = $routes;
    }

    /**
     * Register your components here
     *
     * @return void
     */
    abstract protected function register();
}