<?php

namespace Davek1312\App\Tests;

use Davek1312\App\Tests\Mock\MockRegistry;

class RegistryTest extends \PHPUnit_Framework_TestCase {

    private $registry;

    public function setUp() {
        parent::setUp();
        $this->registry = new MockRegistry();
    }

    public function testRegisterConfig() {
        $this->assertEquals(['config'], $this->registry->getConfig());
    }

    public function testRegisterMigrations() {
        $this->assertEquals(['migration'], $this->registry->getMigrations());
    }

    public function testRegisterCommands() {
        $this->assertEquals(['command'], $this->registry->getCommands());
    }

    public function testRegisterLang() {
        $this->assertEquals(['lang' => 'lang'], $this->registry->getLang());
    }

    public function testRegisterValidationRules() {
        $this->assertEquals(['validation' => 'rule'], $this->registry->getValidationRules());
    }

    public function testRegisterImplicitValidationRules() {
        $this->assertEquals(['implicit-validation' => 'implicit-rule'], $this->registry->getImplicitValidationRules());
    }

    public function testRegisterValidationReplacers() {
        $this->assertEquals(['replacer-validation' => 'replacer'], $this->registry->getValidationReplacers());
    }

    public function testRegisterRoutes() {
        $this->assertEquals(['routes'], $this->registry->getRoutes());
    }
}