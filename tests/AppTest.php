<?php

namespace Davek1312\App\Tests;

use Davek1312\App\App;
use Davek1312\App\Tests\Mock\MockRegistry;

class AppTest extends \PHPUnit_Framework_TestCase {

    private $app;
    private $appConfig;

    public function setUp() {
        parent::setUp();
        $this->appConfig = [
            'root_package_dir' => 'root_package_dir',
            'registry' => [
                MockRegistry::class,
            ],
        ];
        $this->app = new App();
        $this->app->setAppConfig($this->appConfig);
    }

    public function testGetAppConfig() {
        $this->assertEquals($this->appConfig, $this->app->getAppConfig());
    }

    public function testGetConfig() {
        $this->assertEquals(['config'], $this->app->getConfig());
    }

    public function testGetMigrations() {
        $this->assertEquals(['migration'], $this->app->getMigrations());
    }

    public function testGetCommands() {
        $this->assertEquals(['command'], $this->app->getCommands());
    }

    public function testGetLang() {
        $this->assertEquals(['lang' => 'lang'], $this->app->getLang());
    }

    public function testGetRegistry() {
        $this->assertEquals([
            MockRegistry::class
        ], $this->app->getRegistry());
    }

    public function testGetRootPackageDir() {
        $this->assertEquals('root_package_dir', $this->app->getRootPackageDir());
    }

    public function testGetValidationRules() {
        $this->assertEquals(['validation' => 'rule'], $this->app->getValidationRules());
    }

    public function testGetImplicitValidationRules() {
        $this->assertEquals(['implicit-validation' => 'implicit-rule'], $this->app->getImplicitValidationRules());
    }

    public function testGetValidationReplacers() {
        $this->assertEquals(['replacer-validation' => 'replacer'], $this->app->getValidationReplacers());
    }

    public function testGetRoutes() {
        $this->assertEquals(['routes'], $this->app->getRoutes());
    }
}