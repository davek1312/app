<?php

namespace Davek1312\App\Tests\Mock;

use Davek1312\App\Registry;

class MockRegistry extends Registry {

    protected function register() {
        $this->registerConfig('config');
        $this->registerMigrations('migration');
        $this->registerCommands('command');
        $this->registerLang('lang', 'lang');
        $this->registerValidationRules('validation', 'rule');
        $this->registerImplicitValidationRules('implicit-validation', 'implicit-rule');
        $this->registerValidationReplacers('replacer-validation', 'replacer');
        $this->registerRoutes('routes');
    }
}