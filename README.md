# davek1312/app

Management package for davek1312 packages.

# Installation

The package is available on [Packagist](https://packagist.org/packages/davek1312/app),
you can install it using [Composer](https://getcomposer.org/).

```bash
composer require davek1312/app
```

# Registering Components

You can register the following components: [Database Migrations](https://packagist.org/packages/davek1312/database), [Console Commands](https://packagist.org/packages/davek1312/console) and [Configuration Files](https://packagist.org/packages/davek1312/config).

## Create Your Registry Class:
```php
<?php

namespace YourNamespace;

use Davek1312\App\Registry;

class YourRegistry extends Registry {
    
    protected function register() {
        // Register the directory of your configuration files
        $this->registerConfig(__DIR__.'/path/to/config');
        
        // Register the directory of your database migration files
        $this->registerMigrations(__DIR__.'/path/to/migrations');
        
        // Register the class names of your console commands
        $this->registerCommands('YourNamespace\Console\Commands\YourCommand');
        
        // Register the package language files
        $this->registerLang('package-name', __DIR__.'path/to/lang');
        
        // Register custom validation rule using a closure
        $this->registerValidationRules('foo', function ($attribute, $value, $parameters, $validator) {
            return $value == 'foo';
        });
        
        // Register custom validation rule using a method
        $this->registerValidationRules('foo', 'FooValidator@validate');
        
        // Register an implicit custom validation rule
        $this->registerImplicitValidationRules('foo', 'FooValidator@validateImplicit');
        
        // Register a custom validation rule replacer. This is used to replace custom place holders.
        $this->registerValidationReplacers('foo', function($message, $attribute, $rule, $parameters) {
            return str_replace('replace', 'with', $message);
        });
        
        // Register the package routes files
        $this->registerRoutes(__DIR__.'path/to/routes');
    }
}
```

## Register Your Registry Class:
Copy the management folder i.e `vendor/davek1312/app/davek1312` to your application's root directory and add `YourNamespace\YourRegistry` to `app.php`:
```php
<?php

return [
    
     'root_package_dir' => __DIR__.'/../',
    
        'registry' => [
    
            'YourNamespace\YourRegistry',
    
        ],

];
```